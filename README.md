# Recommendation Systems #

For this project you will analyze the interactions that users have with articles on the IBM Watson Studio platform, and make recommendations to them about new articles you think they will like. Below you can see an example of what the dashboard could look like displaying articles on the IBM Watson Platform.

![Tennis](image/ibm_articles.png)


Though the above dashboard is just showing the newest articles, you could imagine having a recommendation board available here that shows the articles that are most pertinent to a specific user. In order to determine which articles to show to each user, we perform a study of the data available on the IBM Watson Studio platform. 

The recommendation methods we explore are :

1.  **Rank Based Recommendations**
We find the most popular articles simply based on the most interactions. Since there are no ratings for any of the articles, it is easy to assume the articles with the most interactions are the most popular. These are then the articles we might recommend to new users (or anyone depending on what we know about them).

2. **User-User Based Collaborative Filtering**
In order to build better recommendations for the users of IBM's platform, we could look at users that are similar in terms of the items they have interacted with. These items could then be recommended to the similar users. This would be a step in the right direction towards more personal recommendations for the users.

3. **Content Based Recommendations** 
Our recommendation system use TF-IDF as text feature extractor. And to simplify computation, we only use doc_description and doc_full_name because the amount of text is less. Then the recommender use dot product as similarity measure. The bigger the value, then the more similar the articles are. Then the recommender will output top-n similar articles. We also notice that there are some article ids that exist in df but not in df_content. So for those article ids, we use rank-based recommendation.

4. **Matrix Factorization** 
We use a machine learning approach to building recommendations. Using the user-item interactions, you will build out a matrix decomposition. Using your decomposition, you will get an idea of how well you can predict new articles an individual might interact with. 


### Acknowledgement ###
1. [Udacity](https://www.udacity.com/)
